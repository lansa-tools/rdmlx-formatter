# rdmlx-formatter

Currently just a PowerShell script that will format a text file in place for indentation and removing double blank lines.

Would be nice to expand to also format spacing of variables, and change the data source to a config file for increasing indents. Perhaps include multi-line by using new lines not supported by Visual LANSA.

Has an issue where if the file is already formated then some sections don't format correctly.

## How To Use

From a shell run the script with the path to the file as the parameter

```powershell
# powershell
.\lansa-formatting.ps1 '..\sample-code\program.rdmlx'

# cmd
powershell -file ".\lansa-formatting.ps1" "..\sample-code\program.rdmlx"
```