param([string]$file)

$increase = @(
    'begin_com',
    'Mthroutine',
    'subroutine',
    'evtroutine',
    'Selectlist',
    'Select_sql',
    'Select',
    'If',
    'If_Null',
    'If_Status',
    'If_Error',
    'Else',
    'for',
    'while',
    'when',
    'otherwise',
    'case',
    'dowhile'
    'Begincheck'
)

$decrease = @(
    'end_com',
    'Endroutine'
    'Else',
    'endif',
    'endwhile',
    'endfor',
    'when',
    'otherwise',
    'endselect',
    'endcase',
    'endcheck'
)

for($i=0; $i -lt $increase.Count; $i++) { $increase[$i]=$increase[$i].ToLower() }
for($i=0; $i -lt $decrease.Count; $i++) { $decrease[$i]=$decrease[$i].ToLower() }

$script:indent = 0
$indentSpace = '    '
$lines = Get-Content $file
$formated = @()
$prevHadBlankLine = $false

function left() {
    if($script:indent -gt 0) {
        $script:indent--
    }
}

function right() {
    $script:indent++
}

function getIndentSpace() {    
    return $indentSpace * $script:indent
}

for($i = 0; $i -lt $lines.Count; $i++) {
    $line = $lines[$i]
    $trimmed = $line.Trim()

    # skip duplicate blank lines
    if($trimmed -eq '') {
        if($prevHadBlankLine) {
            continue
        }
        $prevHadBlankLine = $true;
    } else {
        $prevHadBlankLine = $false
    }

    $words = $line.Split(' ')
    $firstword = $words[0]
    $lastword = $words[$words.Length - 1]

    if($decrease.Contains($firstword.ToLower()) -or $decrease.Contains($lastword.ToLower())) {
        left
    }

    $newLine = (getIndentSpace) + $trimmed
    $formated += $newLine

    if($increase.Contains($firstword.ToLower())) {
        right
    }
}

$formated | Set-Content $file